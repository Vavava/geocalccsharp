using ConsoleApplication.shapes;
using ConsoleApplication.util;

namespace ConsoleApplication.test
{
    using NUnit.Framework;
    
    [TestFixture]
    public class CoordinateHelperTest
    {
        [Test]
        public void testCalculateVectorModule() {
            Point zeroPoint = new Point(0,0);
            Point negativePoint1 = new Point(-1,-2);
            Point negativePoint2 = new Point(-2, -3);
            Point positivePoint1 = new Point(1,2);
            Point positivePoint2 = new Point(2,3);
            Point positivePoint22 = new Point(2, 3);
            Point doublePoint1 = new Point(1.5, 1.5);
            Point doublePoint2 = new Point(2.5, 3.5);

            Assert.AreEqual(CoordinateHelper.CalculateVectorModule(zeroPoint,zeroPoint),0,0.01);
            Assert.AreEqual(CoordinateHelper.CalculateVectorModule(positivePoint2, positivePoint22), 0, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateVectorModule(negativePoint1, negativePoint2),1.41, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateVectorModule(positivePoint1, positivePoint2), 1.41, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateVectorModule(doublePoint1, doublePoint2), 2.23, 0.01);
        }
        
        [Test]
        public void testCalculateScalarMultiplication() {
            Vector zeroVector = new Vector(0,0);
            Vector negativeVector1 = new Vector(-1,-2);
            Vector negativeVector2 = new Vector(-2, -3);
            Vector positiveVector1 = new Vector(1,2);
            Vector positiveVector2 = new Vector(2,3);
            Vector doubleVector1 = new Vector(1.7, 1.5);
            Vector doubleVector2 = new Vector(2.5, 3.5);

            Assert.AreEqual(CoordinateHelper.CalculateScalarMultiplication(zeroVector,zeroVector), 0, 0.1);
            Assert.AreEqual(CoordinateHelper.CalculateScalarMultiplication(negativeVector1, negativeVector2), 8, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateScalarMultiplication(positiveVector1, positiveVector2), 8, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateScalarMultiplication(doubleVector1, doubleVector2), 9.5, 0.01);
        }

        [Test]
        public void testCalculateAngle() {
            Point zeroPoint = new Point(0,0);
            Point positivePoint1 = new Point(1,1);
            Point positivePoint2 = new Point(2,1);
            Point positivePoint3 = new Point(2, 2);
            Point positivePoint4 = new Point(3, 1);
            Point negativePoint1 = new Point(-1,-1);
            Point negativePoint2 = new Point(-2,-2);
            Point negativePoint3 = new Point(-1, -3);
            Point doublePoint1 = new Point(1.5, 1.7);
            Point doublePoint2 = new Point(2.5, 2.3);

            Vector zeroVector = new Vector(zeroPoint, zeroPoint);
            Vector vector1 = new Vector(positivePoint1, positivePoint2);
            Vector vector2 = new Vector(positivePoint1, positivePoint3);
            Vector vector3 = new Vector(positivePoint1, positivePoint4);
            Vector vector4 = new Vector(positivePoint2, positivePoint1);
            Vector vector5 = new Vector(positivePoint2, positivePoint4);
            Vector negativeVector1 = new Vector(negativePoint1, negativePoint2);
            Vector negativeVector2 = new Vector(negativePoint1, negativePoint3);
            Vector doubleVector1 = new Vector(doublePoint1, doublePoint2);
            Vector doubleVector2 = new Vector(doublePoint1, zeroPoint);

            Assert.AreEqual(CoordinateHelper.CalculateAngle(vector1, vector2), 45, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateAngle(vector1, vector3), 0, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateAngle(vector4, vector5), 180, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateAngle(negativeVector1, negativeVector2), 45, 0.01);
            Assert.AreEqual(CoordinateHelper.CalculateAngle(doubleVector1, doubleVector2), 162.39, 0.01);

            //exception.expect(ArithmeticException.class);
            //CoordinateHelper.calculateAngle(zeroVector, zeroVector);
        }
        
    }
}