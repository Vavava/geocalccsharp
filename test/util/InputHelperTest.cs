using ConsoleApplication.shapes;
using ConsoleApplication.util;
using NUnit.Framework;

namespace ConsoleApplication.test
{
    [TestFixture]
    public class InputHelperTest
    {
        [Test]
        public void testParseCoordinates() {
            Point nullPoint = InputHelper.ParseCoordinates("");
            Point stringPoint = InputHelper.ParseCoordinates("string");
            Point commaDividerPoint = InputHelper.ParseCoordinates("11,2; 11,5");
            Point noDecimalPartPoint = InputHelper.ParseCoordinates("11.; 11.");

            Point positivePoint = InputHelper.ParseCoordinates("1; 2");
            Point negativePoint = InputHelper.ParseCoordinates("-1; -2");
            Point doublePoint = InputHelper.ParseCoordinates("11.2; 11.5");

            Assert.IsNull(nullPoint);
            Assert.IsNull(commaDividerPoint);
            Assert.IsNull(noDecimalPartPoint);
            Assert.IsNull(stringPoint);

            Assert.AreEqual(new Point(1,2), positivePoint);
            Assert.AreEqual(new Point(-1,-2), negativePoint);
            Assert.AreEqual(new Point(11.2,11.5), doublePoint);
        }

        [Test]
        public void testIsQuit() {
            Assert.IsTrue(InputHelper.IsQuit("q"));
            Assert.IsFalse(InputHelper.IsQuit("Q"));
            Assert.IsFalse(InputHelper.IsQuit(""));
            Assert.IsFalse(InputHelper.IsQuit("wrong"));
        }
    }
}