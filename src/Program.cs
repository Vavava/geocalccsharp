﻿using System;
using System.Collections.Generic;
using ConsoleApplication.shapes;
using ConsoleApplication.util;

namespace ConsoleApplication
{
    internal class Program
    {
        public static void Exit() {
            OutputHelper.PrintExit();
            Environment.Exit(0);
        }

        public static Point CoordinateReader(string vertexName){
            Point point = null;
            while (point == null) {
                OutputHelper.AskForCoordinate(vertexName);
                string coordinates = Console.ReadLine();
                if(InputHelper.IsQuit(coordinates)) {
                    Exit();
                }
                point = InputHelper.ParseCoordinates(coordinates);
                if(point == null) {
                    OutputHelper.PrintWrongCoordinates();
                }
            }
            return point;
        }

        public static void TriangleReader(){
            OutputHelper.PrintGreetings();
            Point vertexA = CoordinateReader("A");
            Point vertexB = CoordinateReader("B");
            Point vertexC = CoordinateReader("C");
            Triangle triangle = new Triangle(vertexA, vertexB, vertexC);
            OutputHelper.PrintShapeArea(triangle.GetArea());
            OutputHelper.PrintShapePerimeter(triangle.GetPerimeter());
        }       
        
        public static void Main(string[] args)
        {
            while (true) {
                TriangleReader();
            }
        }
    }
}