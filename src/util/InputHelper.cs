using System;
using System.Globalization;
using System.Text.RegularExpressions;
using ConsoleApplication.shapes;

namespace ConsoleApplication.util
{
    public class InputHelper
    {
        public static Point ParseCoordinates(string str) {
            Regex regex = new Regex("^(\\-?\\d+(\\.\\d+)?);\\s*(\\-?\\d+(\\.\\d+)?)$");
            Match match = regex.Match(str);
            if(match.Success)
            {
                var x = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
                var y = double.Parse(match.Groups[3].Value, CultureInfo.InvariantCulture);
                return new Point(x,y);
            }
            return null;
        }

        public static bool IsQuit(string str) {
            return str.Equals("q");
        }
    }
}