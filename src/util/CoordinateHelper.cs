using System;
using ConsoleApplication.shapes;

namespace ConsoleApplication.util
{
    public class CoordinateHelper
    {
        public static double CalculateVectorModule(Point firstPoint, Point secondPoint) {
            Vector vector = new Vector(firstPoint, secondPoint);
            return CalculateVectorModule(vector);
        }

        public static double CalculateVectorModule(Vector vector) {
            return Math.Sqrt(Math.Pow(vector.X,2) + Math.Pow(vector.Y,2));
        }

        public static double CalculateScalarMultiplication(Vector one, Vector two) {
            return one.X * two.X + one.Y * two.Y;
        }

        public static double CalculateAngle (Vector one, Vector two) {
            var firstModule = CalculateVectorModule(one);
            var secondModule = CalculateVectorModule(two);
            if(firstModule.Equals(0) || secondModule.Equals(0)) {
                throw new ArithmeticException("Cannot calculate angle as one of the arguments is zero vector");
            }
            var scalarMultiplication = CalculateScalarMultiplication(one, two);
            return Math.Acos(scalarMultiplication/firstModule/secondModule)*180/Math.PI ;
        }
    }
}