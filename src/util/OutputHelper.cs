using System;

namespace ConsoleApplication.util
{
    public class OutputHelper
    {
        public static void AskForCoordinate(String pointName) {
            Console.WriteLine("Please type coordinates for vertex " + pointName);
        }

        public static void PrintShapeArea(double area) {
            Console.WriteLine("Shape area is " + area.ToString());
        }

        public static void PrintShapePerimeter(double perimeter) {
            Console.WriteLine("Shape perimeter is " + perimeter.ToString());
        }

        public static void PrintExit() {
            Console.WriteLine("Exit!");
        }

        public static void PrintWrongCoordinates() {
            Console.WriteLine("Wrong coordinates");
        }

        public static void PrintGreetings() {
            Console.WriteLine("Let's calculate triangle parameters. " +
                               "Type vertex coordinates in the format '12.75; -56.47'. Type 'q' to quit.");
        }
    }
}