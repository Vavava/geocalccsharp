namespace ConsoleApplication.shapes
{
    public interface IShape
    {
        double GetPerimeter();
        double GetArea();
    }
}