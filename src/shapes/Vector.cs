namespace ConsoleApplication.shapes
{
    public class Vector
    {
        private double x;
        private double y;
        
        public Vector(Point from, Point to) {
            x = to.X - from.X;
            y = to.Y - from.Y;
        }

        public Vector(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }
    }
}