using System;
using ConsoleApplication.util;

namespace ConsoleApplication.shapes
{
    public class Triangle: IShape
    {
        private Point _vertexA;
        private Point _vertexB;
        private Point _vertexC;

        private double _edgeAb;
        private double _edgeBc;
        private double _edgeAc;

        public Triangle(Point vertexA, Point vertexB, Point vertexC) {
            _vertexA = vertexA;
            _vertexB = vertexB;
            _vertexC = vertexC;
            CalculateEdges();
        }

        private void CalculateEdges() {
            _edgeAb = CoordinateHelper.CalculateVectorModule(_vertexA, _vertexB);
            _edgeBc = CoordinateHelper.CalculateVectorModule(_vertexB, _vertexC);
            _edgeAc = CoordinateHelper.CalculateVectorModule(_vertexC, _vertexA);
        }
        public double GetPerimeter()
        {
            return _edgeAb + _edgeBc + _edgeAc;
        }

        public double GetArea()
        {
            var halfPerimeter = GetPerimeter()/2;
            return Math.Sqrt(halfPerimeter
                             *(halfPerimeter - _edgeAb)
                             *(halfPerimeter - _edgeBc)
                             *(halfPerimeter - _edgeAc));
        }
    }
}