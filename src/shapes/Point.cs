using System;

namespace ConsoleApplication.shapes
{
    public class Point
    {
        private double _x;
        private double _y;

        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }


        public override bool Equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;

            Point p = obj as Point;
            if (p == null) return false;

            return (_x == p.X) && (_y == p.Y);
        }
    }
}